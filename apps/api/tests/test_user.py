from django.test import TestCase
from apps.api.models import User


class UserTest(TestCase):
    def setUp(self):
        user_email = 'email@test.com'
        User.objects.create(
            id=9000,
            name=user_email,
            email=user_email,
            password='pass123'
        )
        
    def test_user(self):
        # FIELDS
        user = User.objects.get(id=9000)
        name = user.name
        email = user.email
        password = user.password
        
        self.assertEqual(name,'email@test.com')
        self.assertEqual(email,'email@test.com')
        self.assertEqual(password,'pass123')
    