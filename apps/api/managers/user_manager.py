import logging
# Django
from django.contrib.auth.models import BaseUserManager
from django.db import models, IntegrityError
from django.conf import settings
from django.contrib.auth.hashers import make_password


# Rest Framework
from rest_framework.exceptions import PermissionDenied

logger = logging.getLogger(__name__)

# noinspection SpellCheckingInspection, SqlResolve
class CustomUserManager(BaseUserManager):
    def get_or_create_for_oauth2(self, payload):
        user_key = settings.USERNAME_FIELD

        if user_key in payload:
            # the attribute that uniquely identifies the user in oauth2 external service
            user_unique_attr = payload[user_key]

            # based on project requirements allow create/raise permission denied
            try:
                filters = {user_key:user_unique_attr}
                user = self.get(**filters)

            except self.model.DoesNotExist:
                # raise PermissionDenied('Logged in user does not exist in the system. '
                #                        'Please contact the administrator for more information.')
                try:
                    user_dict = {
                        user_key : user_unique_attr,
                        'name' : payload.get('name',user_unique_attr)
                    }
                    user = self.create(
                        **user_dict
                    )
                except IntegrityError as exc:
                    logger.error('Exception occured creating user: %s',str(exc))
                    raise PermissionDenied('Missing require attributes to create user.')

            if not user.is_active:
                raise PermissionDenied('Logged in user is inactive. '
                                       'Please contact the administrator for more information.')
        else:  # GUEST USER
            user = self.get(email=settings.GUEST_EMAIL)

        return user

    def create_superuser(self,email,password=None,):
        user = self.create(email=email,password=make_password(password))
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class UserManager(models.Manager):
    def check_exists(self, user_id=None, email=None):
        """
        Check user exists by id/email

        :param inr user_id: User id to check if corresponding user exists.
                            If provided email param is redundant
        :param str email: User email to check if corresponding user exists
        :return: True if user exists / False if user is new
        :rtype bool
        """

        if user_id is None and email is None:
            return False

        # Check user exists by id
        if user_id is not None:
            filters = dict(id=user_id)
        else:
            filters = dict(email=email)

        user = (
            super()
            .get_queryset()
            .filter(**filters)
            .first()
        )

        return user is not None

    def get(self, user_id=None, email=None):
        """
        Get user by id/email

        :param int user_id: User id to check if corresponding user exists.
                            If provided email param is redundant
        :param str email: User email to check if corresponding user exists
        :return: User object if exists / otherwise None
        :rtype User
        """

        if user_id is None and email is None:
            return False

        # Check user exists by id
        if user_id is not None:
            filters = dict(id=user_id)
        else:
            filters = dict(email=email)

        return (
            super()
            .get_queryset()
            .filter(**filters)
            .first()
        )
