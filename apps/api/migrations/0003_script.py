from django.db import migrations
from django.core.management import call_command


def load_users(apps, schema_editor):
    call_command('loaddata','user')
    
def delete_users(apps, schema_editor):
    Users = apps.get_model('api', 'User')
    Users.objects.all().delete()

def load_authentication_groups(apps, schema_editor):
    call_command('loaddata','auth_group')
    
def delete_authentication_groups(apps, schema_editor):
    Users = apps.get_model('api', 'User')
    Users.objects.all().delete()
    
def assign_user_groups(apps, schema_editor):
    User = apps.get_model('api','User')
    
    user1 = User.objects.filter(id=2).first()
    user2 = User.objects.filter(id=3).first()
    user3 = User.objects.filter(id=4).first()
    
    user1.groups.add(2)
    user2.groups.add(2)
    user3.groups.add(1)
    
    user1.department_id = 1
    user1.save()
    
    user2.department_id = 2
    user2.save()
    
    
def remove_user_from_groups(apps, schema_editor):
    User = apps.get_model("auth", "User")

    for user in User.objects.all():
        user.groups.set([])


class Migration(migrations.Migration):
    dependencies = [
        ('api', '0002_script')
    ]
    
    operations = [
        migrations.RunPython(load_users, delete_users),
        migrations.RunPython(load_authentication_groups, delete_authentication_groups),
        migrations.RunPython(assign_user_groups, remove_user_from_groups),
    ]