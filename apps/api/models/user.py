# Django
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.validators import validate_email
from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _

# Local App
from apps.api.managers import CustomUserManager, UserManager


class User(AbstractBaseUser, PermissionsMixin):
    """
    Custom authentication User model with admin-compliant permissions.
    """

    USERNAME_FIELD = settings.USERNAME_FIELD
    REQUIRED_FIELDS = []

    # DATABASE FIELDS
    name = models.CharField(max_length=255)
    email = models.EmailField(
        max_length=254,
        blank=False,
        unique=True,
        validators=[validate_email]
    )
    password = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField(
        _('is active'),
        default=True,
        help_text=_('Designates whether the user is active or not.'),
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    department = models.ForeignKey(
        to='student_api.Department',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='user_department'
    )

    # MANAGERS
    objects = CustomUserManager()
    profile = UserManager()

    # META CLASS
    class Meta:
        ordering = ('id',)

    # TO STRING METHOD
    def __str__(self):
        return str(self.name)
