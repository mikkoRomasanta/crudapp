from django.db import models
from .user import User


class TimeStamp(models.Model):
    created_at = models.DateTimeField(
        auto_now_add=True,
        blank=True,
        null=True
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        blank=True,
        null=True
    )
    created_by = models.ForeignKey(
        User,
        blank=True,
        null=True,
        db_column='created_by',
        related_name='%(class)s_created_by',
        on_delete=models.SET_NULL
    )
    updated_by = models.ForeignKey(
        User,
        null=True,
        blank=True,
        db_column='updated_by',
        related_name="%(class)s_updated_by",
        on_delete=models.SET_NULL
    )

    class Meta:
        abstract = True

    def __unicode__(self):
        return 'General Configuration'
