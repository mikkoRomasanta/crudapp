from django.contrib import admin

# Local App
from apps.api.models import User
from .user_admin import UserAdmin


admin.site.register(User,UserAdmin)