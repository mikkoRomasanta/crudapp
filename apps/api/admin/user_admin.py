from django.contrib import admin


class UserAdmin(admin.ModelAdmin):
    list_display = ('id','name','email','is_active','is_staff','department')
    