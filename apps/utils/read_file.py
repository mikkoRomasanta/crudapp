import pandas as pd
# 3rd party
from rest_framework.exceptions import ValidationError


def read_file(file):
    allowed_types = ('.csv','.xlsx','.xls','.xlsb','.xlsm','.odf','.ods','.odt')
    file_name = str(file).lower()

    if not file_name.endswith(allowed_types):
        raise ValidationError('File type not supported')

    f = pd.read_csv if '.csv' in file_name else pd.read_excel

    try:
        df = f(file, index_col=False)
    except UnicodeDecodeError:
        raise ValidationError('Error reading file')

    return df
