from rest_framework import serializers
from apps.project_api.models import Project


class ProjectListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ('id','project_name')


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        exclude = ('created_at','updated_at', 'created_by', 'updated_by')
