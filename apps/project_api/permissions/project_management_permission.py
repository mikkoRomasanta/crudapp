from rest_framework.permissions import BasePermission
# from rest_framework.request import Request
# from rest_framework.views import View


class CanManageProjects(BasePermission):
    """
    Allows only teachers in the same department to manage student projects
    """

    def has_permission(self, request, view) -> bool:
        switcher = {
            'GET': ['project_api.view_project'],
            'HEAD':  ['project_api.view_project'],
            'OPTIONS': ['project_api.view_project'],
            'PUT':  ['project_api.view_project', 'project_api.change_project'],
            'PATCH': ['project_api.view_project', 'project_api.change_project'],
            'DELETE': ['project_api.view_project', 'project_api.delete_project'],
            'POST': ['project_api.add_project'],

        }
        permissions = switcher.get(request.method)

        return request.user.has_perms(permissions)
