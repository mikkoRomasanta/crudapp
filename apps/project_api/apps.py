from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ProjectApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.project_api'
    verbose_name = _('Projects')
