from django.urls import path
from apps.project_api.views import ProjectList, ProjectDetail


app_name = 'project_api'

urlpatterns = [
    path(
        route='<int:pk>/',
        view=ProjectDetail.as_view(),
        name='project_detail'
        ),

    path(
        route='',
        view=ProjectList.as_view(),
        name='project_list'
        ),
]
