from django.test import TestCase
from apps.project_api.models import Project


class ProjectTests(TestCase):
    def setUp(self):
        Project.objects.create(
            id=9000,
            name='Test Project',
            description='Test Description',
            status='review'
        )
        
    def test_project(self):
        # FIELDS
        project = Project.objects.get(id=9000)
        name = project.name
        description = project.description
        status = project.status
        
        # PROPERTIES
        project_name = project.project_name
        
        # TESTS
        self.assertEqual(name, 'Test Project')
        self.assertEqual(description, 'Test Description')
        self.assertEqual(status, 'review')
        self.assertEqual(project_name, 'Test Project - ')