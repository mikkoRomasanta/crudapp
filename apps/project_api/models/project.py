import uuid

# Django
from django.db import models
from ckeditor.fields import RichTextField

# External Apps
from apps.student_api.models import Student
from apps.api.models import TimeStamp


class Project(TimeStamp):
    PROJECT_STATUS = (
        ('planning', 'Planning'),
        ('started', 'Started'),
        ('planning','Review'),
        ('completed', 'Completed')
    )

    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        unique=True
    )
    owner = models.ForeignKey(
        Student,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='projects'
    )
    name = models.CharField(
        max_length=255,
        unique=True
        )
    description = models.TextField(null=True)

    status = models.CharField(
        max_length=10,
        blank=True,
        null=True,
        choices=PROJECT_STATUS,
    )

    html = RichTextField(
        blank=True,
        null=True
    )

    objects = models.Manager()

    @property
    def project_name(self) -> str:
        name = '' if self.name is None else self.name
        owner = '' if self.owner is None else self.owner

        return f'{name} - {owner}'


    class Meta:
        ordering = ('created_at',)

    def __str__(self):
        return str(self.project_name)
