from django.contrib import admin

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('id','name','description','status','owner','created_at')
    actions = ['set_planning','set_started','set_review', 'set_completed']

    def set_planning(modeladmin, request, queryset):
        queryset.update(status='planning')

    def set_started(modeladmin, request, queryset):
        queryset.update(status='started')

    def set_review(modeladmin, request, queryset):
        queryset.update(status='review')

    def set_completed(modeladmin, request, queryset):
        queryset.update(status='completed')