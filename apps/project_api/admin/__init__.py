from django.contrib import admin

# Local App
from apps.project_api.models import Project
from .project_admin import ProjectAdmin


admin.site.register(Project,ProjectAdmin)
