# 3rd party
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
# local apps
from apps.project_api.models import Project
from apps.project_api.serializers import ProjectListSerializer, ProjectSerializer
from apps.project_api.permissions import CanManageProjects


class ProjectList(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated, CanManageProjects]
    serializer_class = ProjectListSerializer
    # queryset = Project.objects.all()

    def get_queryset(self):
        department_id = self.request.user.department_id

        if department_id is None:
            return Project.objects.none()

        # filter from the foreign key's attributes

        return Project.objects.filter(owner__department=department_id)


class ProjectDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated, CanManageProjects]
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
