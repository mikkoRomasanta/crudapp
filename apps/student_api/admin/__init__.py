from django.contrib import admin

# Local App
from apps.student_api.models import Student, Department
from .student_admin import StudentAdmin
from .department_admin import DepartmentAdmin


admin.site.register(Student,StudentAdmin)
admin.site.register(Department,DepartmentAdmin)
