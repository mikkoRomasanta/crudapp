from django.contrib import admin

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('id','name','created_at')
    ordering = ('id',)
