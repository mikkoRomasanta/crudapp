import csv

# Django
from django.contrib import admin
from django.http import HttpResponse

# Local App
from apps.student_api.models import Student


class EnrolledYearListFilter(admin.SimpleListFilter):
    """
    Enrolled Years List Filter

    - Get enrollment_date of students and use the years as a fitler.
    """

    title = 'Enrolled Year'
    parameter_name = 'enrolled_year'

    def lookups(self, request, model_admin):
        students = Student.objects.filter(enrollment_date__isnull=False)
        year_list = [student.enrollment_date.year for student in students]
        year_list = list(set(year_list))

        return [(year, year) for year in year_list]

    def queryset(self, request, queryset):
        value = self.value()

        if value is None:
            return queryset

        queryset = Student.student_objects.enrolled_year(value)

        return queryset


class StudentAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'first_name',
        'last_name',
        'department',
        'is_active',
        'enrollment_date',
        'created_at'
    )
    ordering = ('id',)
    list_filter = ['is_active','department__name', EnrolledYearListFilter]
    search_fields = ['first_name','last_name']
    actions = ['export_as_csv', 'deactivate_student']

    def export_as_csv(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
        writer = csv.writer(response)
        writer.writerow(field_names)

        for obj in queryset:
            row = writer.writerow([getattr(obj, field) for field in field_names])

        return response
