from django.urls import path
from apps.student_api.views import (
    StudentList,
    StudentDetail,
    StudentProjectList,
    EnrolledStudentList,
    UploadStudentsFile,
    DownloadStudentsFile,
)


app_name = 'student_api'

urlpatterns = [
    path(
        route='<int:student_id>/',
        view=StudentDetail.as_view(),
        name='student_detail'
        ),

    path(
        route='',
        view=StudentList.as_view(),
        name='student_list'
        ),

    path(
        route='enrolled/<int:enrolled_year>/',
        view=EnrolledStudentList.as_view(),
        name='student_list_enrolled_at'
    ),

    path(
        route='<int:student_id>/projects/',
        view=StudentProjectList.as_view(),
        name='student_projects'
    ),

    path(
        route='upload/',
        view=UploadStudentsFile.as_view(),
        name='student_upload'
    ),

    path(
        route='download/',
        view=DownloadStudentsFile.as_view(),
        name='student_download'
    ),

]
