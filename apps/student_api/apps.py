from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class StudentApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.student_api'
    verbose_name = _('Students')

    def ready(self):
        import apps.student_api.signals
