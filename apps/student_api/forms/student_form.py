# Django
from django import forms

# Local App
from apps.student_api.models import Student


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student