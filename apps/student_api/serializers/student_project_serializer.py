from rest_framework import serializers
from apps.project_api.models import Project


class StudentProjectSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField()

    class Meta:
        model = Project
        fields = ('id','uuid','name','status', 'first_name')

# class StudentProjectSerializer(serializers.Serializer):
#     id = serializers.IntegerField()
#     first_name = serializers.CharField()
#     name = serializers.CharField()

#     class Meta:
#         fields = ('id','first_name','name')
