import pandas as pd

# 3rd party
from rest_framework import serializers
from copy import deepcopy
#local
from apps.student_api.models import Student



class StudentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id','uuid','full_name')


class StudentCreateSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(required=True, allow_null=False, min_length=3)
    last_name = serializers.CharField(required=True, allow_null=False, min_length=3)
    department_id = serializers.IntegerField(required=False, allow_null=True)


    class Meta:
        model = Student
        fields = ('id','first_name', 'last_name', 'department_id', 'enrollment_date')


    def validate(self, attrs):
        if attrs.get('first_name').isnumeric() or attrs.get('last_name').isnumeric():
            raise serializers.ValidationError(
                'Invalid name.'
            )

        return attrs

    def to_internal_value(self, data):
        payload = deepcopy(data)

        # Change the date received into the format accepted by db
        if data.get('enrollment_date') is not None:
            try:
                payload['enrollment_date'] = pd.to_datetime(
                    data['enrollment_date']
                ).strftime('%Y-%m-%d')
            except(ValueError, TypeError):
                payload['enrollment_date'] = data['enrollment_date']

        return super().to_internal_value(payload)


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        exclude = ('created_at','updated_at', 'created_by', 'updated_by')

    def to_representation(self, instance):
        # Add additional data to the StudentDetail response
        student_data = super().to_representation(instance)

        student_data['full_name'] = f'{student_data["first_name"]} {student_data["last_name"]}'
        student_data['department_name'] = self.context.get('department_name','')

        return student_data

    def to_internal_value(self, data):
        payload = deepcopy(data)

        # Change the date received into the format accepted by db
        if data.get('enrollment_date') is not None:
            try:
                payload['enrollment_date'] = pd.to_datetime(
                    data['enrollment_date']
                ).strftime('%Y-%m-%d')
            except(ValueError, TypeError):
                payload['enrollment_date'] = data['enrollment_date']

        return super().to_internal_value(payload)
