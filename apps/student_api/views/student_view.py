# django
from django.shortcuts import get_object_or_404
# 3rd party apps
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import NotFound
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
# local apps
from apps.student_api.permissions import CanManageStudents
from apps.student_api.models import Student, Department
from apps.student_api.serializers import (
    StudentSerializer,
    StudentListSerializer,
    StudentCreateSerializer
)


class StudentList(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated, CanManageStudents]
    serializer_class = StudentListSerializer

    def get(self, request, *args, **kwargs):
        """
        List all students

        List all students in the department of the requestor
        """
        response = super().get(request, *args, **kwargs)
        return response

    def get_queryset(self):
        # can be used for null values # add to service?
        department_id = self.request.user.department_id
        # department_id = self.request.user.department.id #500 error if null

        enrolled_year = self.kwargs.get('enrolled_year')
        is_dean = self.request.user.groups.filter(name='Dean')

        # Deans are allowed to view students
        if department_id is None and is_dean:
            if enrolled_year:
                return Student.student_objects.enrolled_year(enrolled_year).order_by('id')

            return Student.student_objects.all().order_by('id')

        if enrolled_year:
            return Student.student_objects.enrolled_year(
                enrolled_year, department_id
            ).order_by('id')

        # return Student.student_objects.filter(department=department_id).order_by('id')
        return Student.student_objects.get_students(department_id).order_by('id')

    @swagger_auto_schema(request_body=StudentCreateSerializer)
    def post(self, request, *args, **kwargs):
        """
        Create student

        Creates a student in the department of the requestor
        """

        student_dict = dict(
            first_name = request.data['first_name'],
            last_name = request.data['last_name']
        )

        if 'enrollment_date' in request.data:
            student_dict.update({'enrollment_date': request.data['enrollment_date']})

        serializer = StudentCreateSerializer(data=student_dict)

        if 'department_id' in request.data:
            department_id = request.data['department_id']
        else:
            department_id = self.request.user.department_id

        if serializer.is_valid():
            student = serializer.save(
                department_id = department_id,
                created_by = self.request.user,
                updated_by = self.request.user,
            )
        else:
            raise ValidationError(serializer.errors)

        response = dict(
            id = student.id,
            first_name = student.first_name,
            last_name = student.last_name,
            department_id = student.department_id,
            enrollment_date = student.enrollment_date
        )

        return Response(response, status=status.HTTP_201_CREATED)

    # def perform_create(self, serializer):
    #     # Set the created_by and updated_by field when creating a new student
    #     serializer.save(
    #         created_by = self.request.user,
    #         updated_by = self.request.user
    #     )


class StudentDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated, CanManageStudents]
    serializer_class = StudentSerializer

    def get_object(self):
        department_id = self.request.user.department_id

        if department_id is None:
            raise NotFound

        filters = {
            "pk": self.kwargs.get('student_id'),
            "department_id": department_id
        }

        return get_object_or_404(Student, **filters)

    def get_serializer_context(self):
        # Pass the department object to the serializer
        context = super().get_serializer_context()
        try:
            department = Department.objects.get(id=self.request.user.department_id)
            department = department.__dict__
        except Department.DoesNotExist:
            department = {'name': None}

        return {
            'request': context.get('request'),
            'department_name': department['name']
        }

    def perform_update(self, serializer):
        """
        Set the updated_by field when updating a student
        """
        serializer.save(
            updated_by = self.request.user
        )

class EnrolledStudentList(StudentList):
    http_method_names = ['get']

    swagger_params = [
        openapi.Parameter(
            "enrolled_year",
            openapi.IN_PATH,
            type=openapi.TYPE_INTEGER
        )
    ]

    @swagger_auto_schema(
        manual_parameters=swagger_params,
        operation_id='student_enrolled_at_list'
        )
    def get(self, request, *args, **kwargs):
        """
        List all students enrolled in a certain year and belongs in the department of the requestor
        """
        response = super().get(request, *args, **kwargs)
        return response
