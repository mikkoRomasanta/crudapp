from .student_view import StudentDetail, StudentList, EnrolledStudentList
from .student_project_view import StudentProjectList
from .student_file_view import UploadStudentsFile, DownloadStudentsFile
