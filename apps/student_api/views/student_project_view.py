# django
from django_filters import rest_framework as filters
# 3rd party
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
# local
from apps.student_api.models import Student
from apps.student_api.serializers import StudentProjectSerializer
from apps.student_api.permissions import CanManageStudents
from apps.project_api.permissions import CanManageProjects


class StudentProjectList(generics.ListAPIView):
    permission_classes = [IsAuthenticated, CanManageStudents, CanManageProjects]
    serializer_class = StudentProjectSerializer

    filter_backends = (filters.DjangoFilterBackend,)

    swagger_params = [
        openapi.Parameter(
            "student_id",
            openapi.IN_PATH,
            type=openapi.TYPE_INTEGER
        )
    ]

    @swagger_auto_schema(
        manual_parameters=swagger_params,
        operation_id='student_project_list'
        )
    def get(self, request, *args, **kwargs):
        """
        List all student's project

        List all projects belonging to a student
        """
        response = super().get(request, *args, **kwargs)
        return response

    def get_queryset(self):
        department_id = self.request.user.department_id
        student_id = self.kwargs.get('student_id')

        # return Student.student_objects.get_projects(student_id,department_id).order_by('id')
        return Student.student_objects.get_projects_raw(student_id,department_id)
