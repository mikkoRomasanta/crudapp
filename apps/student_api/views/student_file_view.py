import pandas as pd
# django
from django.http import HttpResponse
# 3rd party apps
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
# from drf_yasg.utils import swagger_auto_schema
# from drf_yasg import openapi

# local apps
from apps.student_api.permissions import CanManageStudents
from apps.student_api.models import Student
from apps.student_api.serializers import  StudentCreateSerializer
from apps.utils import read_file


class UploadStudentsFile(APIView):
    permission_classes = [IsAuthenticated, CanManageStudents]

    def post(self, request, *args, **kwargs):
        """
        Upload students

        Create students from file
        """

        # if not 'student' in request.FILES:
        if not 'student' in request.FILES:
            raise ValidationError('File key expected: student')

        file = request.FILES['student']

        df = read_file(file)
        # student_df = pd.read_csv(file, delimiter=',', index_col=False)

        data = df.to_dict(orient='records')
        serializer = StudentCreateSerializer(data=data, many=True)

        if serializer.is_valid():
            serializer.save()
        else:
            raise ValidationError(serializer.errors)
        # for _, student in df.iterrows():
        #     student_data = Student(
        #         first_name=student['first_name'],
        #         last_name=student['last_name'],
        #         department_id=student['department_id'],
        #         enrollment_date=student['enrollment_date']
        #     )

        #     serializer = StudentCreateSerializer(data=student_data.__dict__)

        #     if not serializer.is_valid():
        #         response = {
        #             "message": f"Invalid data at row {_+1}"
        #         }
        #         return Response(response, status=status.HTTP_400_BAD_REQUEST)

        #     data.append(student_data)

        # Student.student_objects.bulk_create(data)

        response = {
            "message": f"[{len(df)}] students uploaded."
        }

        return Response(response, status=status.HTTP_201_CREATED)

    # @staticmethod
    # def read_file(file):
    #     allowed_types = ('.csv','.xlsx','.xls','.xlsb','.xlsm','.odf','.ods','.odt')
    #     file_name = str(file).lower()

    #     if not file_name.endswith(allowed_types):
    #         raise ValidationError('File type not supported')

    #     f = pd.read_csv if '.csv' in file_name else pd.read_excel

    #     try:
    #         df = f(file, index_col=False)
    #     except UnicodeDecodeError:
    #         raise ValidationError('Error reading file')

    #     return df


class DownloadStudentsFile(APIView):
    def get(self, request, *args, **kwargs) -> HttpResponse:
        """
        Download students

        Download students to file
        """

        students = Student.student_objects.all().order_by('id')
        students_df = pd.DataFrame.from_records(students.values())

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=filename.csv'

        students_df.to_csv(path_or_buf=response)

        return response
