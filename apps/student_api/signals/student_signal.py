from datetime import datetime
#django
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import send_mail
# local app
from apps.student_api.models import Student


@receiver(post_save, sender=Student)
def notify_teacher_of_new_student(sender, instance, **kwargs):
    """
    Notifies teachers of the creation of a student in their department
    """

    date = datetime.now()

    # python -m smtpd -n -c DebuggingServer localhost:1025
    send_mail(
        subject=f'New user created. [{instance}]',
        message=f'User {instance} has been created. {date}',
        from_email='sample@example.com',
        recipient_list=['test@example.com']
    )
