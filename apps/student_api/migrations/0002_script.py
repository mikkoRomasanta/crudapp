from django.db import migrations
from django.core.management import call_command


def load_departments(apps, schema_editor):
    call_command('loaddata','department')
    
def delete_departments(apps, schema_editor):
    Department = apps.get_model('student_api', 'Department')
    Department.objects.all().delete()

def load_students(apps, schema_editor):
    call_command('loaddata','student')
    
def delete_students(apps, schema_editor):
    Student = apps.get_model('student_api', 'Student')
    Student.objects.all().delete()


class Migration(migrations.Migration):
    dependencies = [
        ('student_api', '0001_initial')
    ]
    
    operations = [
        migrations.RunPython(load_departments, delete_departments),
        migrations.RunPython(load_students, delete_students)
    ]