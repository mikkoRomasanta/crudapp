from django.test import TestCase
from apps.student_api.models import Department


class DepartmentTest(TestCase):
    def setUp(self):
        Department.objects.create(
            id=9000,
            name='Test Department'
            )
        
    def test_department(self):
        # FIELDS
        department = Department.objects.get(id=9000)
        name = department.name
        
        # TESTS
        self.assertEqual(name, 'Test Department')
    
        