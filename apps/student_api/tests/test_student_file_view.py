import pandas as pd
import os
import csv
import io

from rest_framework.test import APITestCase, force_authenticate, APIRequestFactory
from rest_framework import status
from apps.student_api import views
from apps.api.models import User
from apps.student_api.models import Student


class StudentFileTests(APITestCase):
    fixtures = ['student_test_data.json']
    
    def setUp(self):
        self.factory = APIRequestFactory()
        self.mock_id = 9000
        self.user = User.objects.get(id=self.mock_id)
        self.students_to_upload = pd.DataFrame(
            {
                'first_name': ['Foo', 'Mock'],
                'last_name': ['Bar', 'Test'],
                'department_id': [1,2],
                'enrollment_date': ['2022-01-01', '2022-02-22']
            }
        )
        self.file_name = (
            f'{os.path.dirname(os.path.dirname(os.path.abspath(__file__)))}'
            f'/test_student_upload.csv'
        )
        self.test_csv = self.students_to_upload.to_csv(self.file_name)
        
    def tearDown(self) -> None:
        self.factory = None
        self.mock_id = None
        self.students_to_upload = None
        self.user = None

        try:
            os.remove(self.file_name)
        except PermissionError:
            pass

        self.file_name = None
    
    def test_upload_file(self):
        """
        - Ensure that upload is done succesfully
        - Ensure that all students are created as Student objects
        - Ensure all fields are set correctly
        """
        view = views.UploadStudentsFile.as_view()
        
        students = open(self.file_name)

        request = self.factory.post(
            '/student/upload/',
            {
                'student': students
            },
            format='multipart'
        )
        force_authenticate(request, user=self.user)
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
        for _, student in self.students_to_upload.iterrows():
            new_student = Student.student_objects.filter(
                first_name=student['first_name'],
                last_name=student['last_name']
            ).first()
            
            # check if student was created
            self.assertIsNotNone(new_student)
            
            # check that fields are set correctly
            if new_student:
                self.assertEqual(new_student.first_name, student['first_name'])
                self.assertEqual(new_student.last_name, student['last_name'])
                self.assertEqual(new_student.department_id, student['department_id'])
                self.assertEqual(str(new_student.enrollment_date), student['enrollment_date'])
                self.assertIsNotNone(new_student.uuid)
                
    def test_download_file(self):
        """
        - Ensure file is found;
        - Ensure filename is correct and added in content-disposition;
        - Ensure column names are the expected ones;
        """
        view = views.DownloadStudentsFile.as_view()
        
        request = self.factory.get('/student/download/')
        force_authenticate(request, user=self.user)
        response = view(request)
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        mock_headers = 'attachment; filename=filename.csv'
        mock_columns = [
            '', 'id', 'created_at', 'updated_at', 'created_by_id', 'updated_by_id', 
            'uuid', 'first_name', 'last_name', 'is_active', 'enrollment_date', 'department_id'
        ]
        
        content = response.content.decode('utf-8')
        csv_reader = csv.reader(io.StringIO(content))
        body = list(csv_reader)
        file_columns = body.pop(0)
        
        # Test filename is correct and added in content-disposition
        self.assertEqual(mock_headers, response.headers.get('content-disposition'))
        # test column names are the expected ones
        self.assertEqual(file_columns, mock_columns)