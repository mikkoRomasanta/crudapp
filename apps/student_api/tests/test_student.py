from django.test import TestCase
from apps.student_api.models import Student


class StudentTest(TestCase):
    def setUp(self):
        Student.objects.create(
            id=9000,
            first_name='test',
            last_name='user'
        )
        
    def test_student(self):
        # FIELDS
        student = Student.objects.get(id=9000)
        uuid = student.uuid
        first_name = student.first_name
        last_name = student.last_name
        is_active = student.is_active
        
        # PROPERTIES
        full_name = student.full_name
        
        # TESTS
        self.assertIsNotNone(uuid)
        self.assertEqual(first_name, 'test')
        self.assertEqual(last_name, 'user')
        self.assertEqual(is_active, 1)
        self.assertEqual(full_name, 'test user')