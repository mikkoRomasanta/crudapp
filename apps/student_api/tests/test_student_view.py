from rest_framework.test import APITestCase, force_authenticate, APIRequestFactory
from rest_framework import status

from apps.student_api import views
from apps.student_api.models import Student, Department
from apps.api.models import User


class StudentTests(APITestCase):
    fixtures = ['student_test_data.json']
    
    def setUp(self):
        self.factory = APIRequestFactory()
        self.mock_id = 9000
        self.wrong_id = 9009
        self.student = Student.student_objects.get(id=self.mock_id)
        self.user = User.objects.get(id=self.mock_id)
        
    def tearDown(self) -> None:
        self.factory = None
        self.mock_id = None
        self.wrong_id = None
        self.student = None
        self.user = None
    
    def test_get_student_list(self):
        """
        Test get student list
        
        - Ensure that the user has access to the data provided they share the same department, otherwise return 401
        - Ensure data returned is a list
        - Ensure the response data is the same as the data in the system
        """
        view = views.StudentList.as_view()
        request = self.factory.get('/student/', format='json')
        
        force_authenticate(request, user=self.user)
        response = view(request)
         
        # Ensure that teachers can access students
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(isinstance(response.data, list))
        
        students = response.data
        
        # Ensure that user can only get students in the same department
        self.assertEqual(len(students), 1)
        
        response_student = students[0]
        
        self.assertTrue(isinstance(response_student, dict))
        
        internal_student = self.student.__dict__
        internal_student['full_name'] = self.student.full_name
        
        for attr in response_student.keys():
            self.assertEqual(str(response_student[attr]), str(internal_student[attr]))
    
    def test_get_student_detail(self):
        """
        Test get student detail
        
        - Ensure that the user has access to the data provided they share the same department, otherwise return 401
        - Ensure data returned is a dict
        - Ensure that the response data is the same as the data in the system
        """
        
        view = views.StudentDetail.as_view()
        request = self.factory.get(f'/student/{self.mock_id}/', format='json')
        
        force_authenticate(request, user=self.user)
        response = view(request, student_id=self.mock_id)
        
        # Ensure that teachers can access students
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(isinstance(response.data, dict))
        
        student = response.data
        # Ensure that the teachers can access the correct student
        internal_student = self.student.__dict__
        internal_student['department'] = self.mock_id
        
        department = Department.objects.get(id=internal_student['department'])
        
        internal_student['department_name'] = department.name
        internal_student['full_name'] = f'{internal_student["first_name"]} {internal_student["last_name"]}'
        
        for attr in student.keys():
            self.assertEqual(str(student[attr]), str(internal_student[attr]))
        
    def test_get_student_with_wrong_id(self):
        """
        Test get student with wrong id
        
        - Ensures that calling the wrong id/enpoint results in an error
        - Ensures that trying access student in a different deparment results in an error
        """
        view = views.StudentDetail.as_view()
        request = self.factory.get(f'/student/{self.wrong_id}', format='json')
        
        force_authenticate(request, user=self.user)
        response = view(request, student_id=self.wrong_id)
        
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        
    def test_update_student_by_id(self):
        """
        Test update student by id
        
        - Ensure that the user has access to the student of the same department, otherwise return 401
        - Ensure that the changes to the user data are reflected
        """
        view = views.StudentDetail.as_view()
        data = {
            "first_name": "Test",
            "last_name": "Put"
        }
        request = self.factory.put(f'/student/{self.mock_id}/', format='json', data=data)
        
        force_authenticate(request, user=self.user)
        response = view(request, student_id=self.mock_id)
        
        # Ensure that the teacher can access the student
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        internal = Student.objects.get(id=self.mock_id)
        internal = internal.__dict__
        student = response.data
        check_attrs = ['first_name', 'last_name']
        
        # Ensure that the changes are reflected
        for attr in check_attrs:
            self.assertEqual(student[attr], data[attr], internal[attr])
            
    def test_create_student(self):
        view = views.StudentList.as_view()
        
        data = {
            "first_name": "Mock",
            "last_name": "Test"
        }
        
        request = self.factory.post('/student/', data=data)
        
        force_authenticate(request, user=self.user)
        response = view(request)
        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        
        student_data = response.data
        student = Student.student_objects.filter(
            first_name=student_data['first_name'],
            last_name=student_data['last_name']
        ).first()
        
        # check that fields are set correctly
        self.assertEqual(student.first_name, data['first_name'])
        self.assertEqual(student.last_name, data['last_name'])
        self.assertEqual(student.department_id, self.user.id)
        self.assertEqual(student.enrollment_date, None)
        self.assertIsNotNone(student.uuid)