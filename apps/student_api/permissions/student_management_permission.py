from rest_framework.permissions import BasePermission
# from rest_framework.request import Request
# from rest_framework.views import View


class CanManageStudents(BasePermission):
    """
    Allows only teachers in the same department to manage students
    """

    # def has_permission(self, request, view):
    #     # group = request.user.groups.all()[0]
    #     permissions = ["student_api.view_student"]
    #     can_view = "student_api.view_student"

    #     return request.user.has_perms(permissions)

    def has_permission(self, request, view) -> bool:
        switcher = {
            'GET': ['student_api.view_student'],
            'HEAD':  ['student_api.view_student'],
            'OPTIONS': ['student_api.view_student'],
            'PUT':  ['student_api.view_student', 'student_api.change_student'],
            'PATCH': ['student_api.view_student', 'student_api.change_student'],
            'DELETE': ['student_api.view_student', 'student_api.delete_student'],
            'POST': ['student_api.add_student'],

        }
        permissions = switcher.get(request.method)

        return request.user.has_perms(permissions)
