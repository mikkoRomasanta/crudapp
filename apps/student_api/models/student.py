import uuid
#django
from django.db import models
from django.utils.translation import gettext_lazy as _
#local
from apps.api.models import TimeStamp
from apps.student_api.managers import StudentObjects
from apps.student_api.models import Department


class Student(TimeStamp):
    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        unique=True
    )
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    is_active = models.BooleanField(
        default=True,
        help_text=_('Designates whether the user is active or not.')
    )
    enrollment_date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        blank=True,
        null=True
    )
    department = models.ForeignKey(
        Department,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='students'
    )

    objects = models.Manager() # default manager
    student_objects = StudentObjects() # custom manager to only show actives students

    @property
    def full_name(self) -> str:
        first_name = '' if self.first_name is None else self.first_name
        last_name = '' if self.last_name is None else self.last_name

        return f'{first_name} {last_name}'

    class Meta:
        ordering = ('created_at',)

    def __str__(self):
        return str(self.full_name)
