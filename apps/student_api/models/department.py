from django.db import models
from apps.api.models import TimeStamp


class Department(TimeStamp):
    name = models.CharField(max_length=255)

    def __str__(self):
        return str(self.name)
