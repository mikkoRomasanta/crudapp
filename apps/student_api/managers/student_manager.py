from django.db import models
from django.shortcuts import Http404


class StudentObjects(models.Manager):
    def get_queryset(self):
        """
        Get all students.
        """
        return super().get_queryset().filter(is_active=1).all()

    def get_students(self, department_id):
        """
        Get all students. Filtered per teacher's department
        """
        return super().get_queryset().filter(
            is_active=1,
            department_id=department_id
        )

    def enrolled_year(self, year, department_id=None):
        """
        Get students who enrolled in a specific year
        """
        if department_id:
            return super().get_queryset().filter(
                enrollment_date__year=year,
                department_id=department_id
            )

        return super().get_queryset().filter(
                enrollment_date__year=year
            )

    def get_projects(self,student_id,department_id):
        """
        Returns all projects that belong to the user
        """
        student = (
            super()
            .get_queryset()
            .filter(
                id=student_id,
                department_id=department_id
            )
            .prefetch_related('projects')
            .first()
        )

        if student is None:
            raise Http404

        return student.projects.all()

    def get_projects_raw(self, student_id, department_id):
        """
        Returns all projects that belong to the user using raw SQL
        """

        query = (
            f'SELECT p.id, p.name, s.first_name FROM project_api_project AS p '
            f'INNER JOIN student_api_student AS s ON p.owner_id=s.id '
            f'WHERE s.department_id = {department_id} AND '
            f'p.owner_id = {student_id} '
            f'ORDER BY p.id ASC'
        )

        return self.raw(query)
