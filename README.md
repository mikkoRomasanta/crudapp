# crudapp

## Requirements

    This app uses Python version 3.8.3

## Installation

1. **Clone git repository**:

    ```bash
    https://gitlab.com/mikkoRomasanta/crudapp.git
    ```

2. **Create virtual environment**

    - Create virtual environment
    - Install requirements
        ```bash
        pip install -r requirements.txt
        ```

3. **Add Environment variables**

    - add components + environments folder under school/settings/

4. **Run migrations**
    - make sure your database is up and running
    - an admin user will automatically be created

## Run

-   Upon running of server, these endpoints will be exposed
    -   [Endpoints](http://localhost:24/api/)
    -   [Documentation](http://localhost:24/docs/)
    -   [Administration](http://localhost:24/admin/)

## Project standards

1. **Latest features**

    - String format is done with the latest python formatters:
        ```python
        variable_name = 'world'
        f'Hello {variable_name}!'
        ```
    - keep libraries up to date (mostly django and DRF)

2. **Best practices**

    - maintain code quality with pylint to 10/10
    - use '' not "" for string formatters (not required but to keep a standard)
    - leave an empty line after docstring - `Code Readability`.
    - always specify in comments parameter type before its name - `Maintainance`
    - always give a default value to function parameters and class arguments - `Testing`
    - always store external imports in .**init**.py files - `Maintanance`
    - always inspect code before deploy and fix PEP warnings or suppress the irrelevant ones - `Code Quality`
    - do not use as line split '\'. Use () - `Code Readability and Quality`

    - Model design standard documentation:

        - [Designing Better Models](https://simpleisbetterthancomplex.com/tips/2018/02/10/django-tip-22-designing-better-models.html)
        - [Two Scoops of Django 1.11] (Daniel Greenfeld)

    - Django admin design documentation:
        - [Django Admin Cookbook](https://buildmedia.readthedocs.org/media/pdf/django-admin-cookbook/latest/django-admin-cookbook.pdf)
