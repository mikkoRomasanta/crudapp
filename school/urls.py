"""school URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

# third party apps
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

# local
from . import views


schema_view = get_schema_view(
   openapi.Info(
      title="School API",
      default_version='v1',
   ),
   validators=['flex', 'ssv'],
   public=False,
   permission_classes=(permissions.IsAuthenticated,),
)

admin.site.site_header = 'CRUDApp'
admin.site.site_title = "CRUDApp"
admin.site.index_title = "Welcome to CRUDApp"

urlpatterns = [
    path(
        route='admin/',
        view=admin.site.urls,
        name='admin_management'
    ),

    path(
        route='docs/',
        view=schema_view.with_ui('redoc', cache_timeout=0),
        name='api_docs'
    ),

    path(
        route='api-auth/',
        view=include('rest_framework.urls'),
        name='api_auth'
    ),

    path(
        route='api/student/',
        view=include('apps.student_api.urls'),
        name='student_api'
    ),

    path(
        route='api/project/',
        view=include('apps.project_api.urls'),
        name='project_api'
    ),

    path(
        route="",
        view=views.index,
        name="index"
    ),

    # AUTH0
    path(
        route="login",
        view=views.login,
        name="login"
    ),

    path(
        route="logout",
        view=views.logout,
        name="logout"
    ),

    path(
        route="callback",
        view=views.callback,
        name="callback"
    ),
]