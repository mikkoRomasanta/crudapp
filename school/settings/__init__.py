import os

from split_settings.tools import optional, include
from dotenv import load_dotenv


load_dotenv('school/settings/environments/.env')

ENV = 'local'

base_settings = [
    'components/common.py',
    'components/auth0.py',
    'components/rest_framework.py',
    'components/application.py',
    'components/databases.py',
    
    #env
    'environments/{}.py'.format(ENV),
    
]

include(*base_settings)